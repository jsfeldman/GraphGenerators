export default class TOFFunctions {

	static theoreticalEq (time, length, exVolt) {
		const amu = 1.66055 * Math.pow(10, -27);
		const charge = 1.602 * Math.pow(10, -19);
		const kgTokDa = ((kg) => ((kg/amu)/1000));

		return kgTokDa((charge * exVolt) / (2 * Math.pow((length / 2), 2)) * Math.pow(time, 2));
	}
}
