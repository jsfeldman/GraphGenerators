const kmeans = require("node-kmeans");

export default class kMeans {

	constructor() {

		const clusterFunc = (err, res) => {
			if (err) console.log(err);
			else console.log("%o", res);
		};

		const clusters = kmeans.clusterize(array, {k: 5}, clusterFunc);

		const clustPointsList = [];
		const centrPoints = [];
		for(const [index, group] of clusters.groups.entries()) {
			const clustPoints = [];
			for(const point of group.cluster) {
				clustPoints.push(new Point(point[0], point[1]));
			}
			const pointsTest = new Points("group" + index, "xAxis", "yAxis", ...clustPoints);
			clustPointsList.push(pointsTest);
			centrPoints.push(new Point(group.centroid[0], group.centroid[1]));
		}
		clustPointsList.push(
			new Points("centroids", "xAxis", "yAxis", ...centrPoints)
		);
		const multiPts = new MultiPts(
			channelName, "xAxis", "yAxis", ...clustPointsList
		);

		const mPGraphInfo = new MultiPtsGraphInfo(
			"800", "400", "#FFFFFF", "#000000", ...pointsInfos
		);
		const channelGraph = new MultiPtsGraph(canvasId, mPGraphInfo);
	}
	
}
