const clusterfck = require("tayden-clusterfck");

import Graph from "./../Graphs/Graph";
import Points from "./../Points/Points";
import Point from "./../Points/Point";
import MultiPts from "./../Points/MultiPts";

export default class HClustering {

	static getClusters(points) {
		
		const [...array] = points.array();

		const clusters = clusterfck.hcluster(array);
		const clustersList = clusters.clusters(6);

		const pointsList = [];
		for(const [index,cluster] of clustersList.entries()) {
			const pointList = [];
			const title = "cluster" + index;
			for(const point of cluster) {
				pointList.push(new Point(point[0], point[1]));
			}
			const color = Points.colors()[index];
			console.log(color);
			pointsList.push(
				new Points(pointList, 
					{color: color, title: title}
				)
			);
		}
		const multiPts = new MultiPts(pointsList, 
			{title: points.title, xAxis: points.xAxis, yAxis: points.yAxis}
		);
		
		const canvas = document.createElement("canvas");
		const canvasId = points.title + "plot";
		canvas.id = canvasId;
		canvas.width = "800";
		canvas.height = "400";
		document.body.append(canvas);

		const graph = new Graph(canvasId);
		graph.dataset = multiPts;
	}
}
