const clusterfck = require("tayden-clusterfck");
import "babel-polyfill";

import Points from "./../Points/Points";
import Point from "./../Points/Point";
import MultiPts from "./../Points/MultiPts";
import MultiPtsDTO from "./../Points/MultiPtsDTO";

onmessage = function (e) {
	console.log("working on it");
	const points = e.data;
	const array = points.points;

	const scaledDist = (v1, v2) => {
		const scaledXDiff = (v1[0] - v2[0])/(points.highestX - points.lowestY);
		const scaledYDiff = (v1[1] - v2[1])/(points.highestY - points.lowestY);
		return Math.sqrt(Math.pow(scaledXDiff + scaledYDiff, 2));
	};

	const clusters = clusterfck.hcluster(array, "manhattan", "average");
	const clustersList = clusters.clusters(10);

	const pointsList = [];
	for(const [index,cluster] of clustersList.entries()) {
		const pointList = [];
		const title = "cluster" + index;
		for(const point of cluster) {
			pointList.push(new Point(point[0], point[1]));
		}
		const color = Points.colors()[index];
		pointsList.push(
			new Points(pointList, 
				{color: color, title: title}
			)
		);
	}
	const multiPts = new MultiPts(pointsList, 
		{title: points.title, xAxis: points.xAxis, yAxis: points.yAxis}
	);
	const multiPtsDTO = new MultiPtsDTO(multiPts);
	console.log("finished");
	postMessage(multiPtsDTO);
};
