const clustering = require("density-clustering");

export default class DensityClustering {

	constructor(points) {

		const [...array] = points.array();
		const dbscan = new clustering.DBSCAN();
		const clusters = dbscan.run(array, 6, 100);
		console.log("done");
		const indexToPoint = (pointIndex) => {
			const arrayPoint = array[pointIndex];
			return new Point(arrayPoint[0],arrayPoint[1]);
		};
		
		const xAxis = "m/z (kTh)"
		const yAxis = "Relative Energy Deposited in Detector";
		const clustPointList = [];
		for(const [index, cluster] of clusters.entries()) {
			const title = "cluster" + index;
			const clustPoints = cluster.map(indexToPoint);
			clustPointList.push(
				new Points(clustPoints, {title: title, 
					color: PointsInfo.colors.next(), xAxis: xAxis, yAxis: yAxis}
				);
			);
		}
		const multiPts = new MultiPts(clustPointList, {title: points.title, 
			xAxis: xAxis, yAxis: yAxis}
		);
	}
}
