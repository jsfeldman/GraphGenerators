import PointsDTO from "./PointsDTO";
import MultiPts from "./MultiPts";

export default class MultiPtsDTO extends PointsDTO{
	
	constructor(multiPts) {
		super(multiPts);
		const pointsDTOList = [];
		for(const points of multiPts.points()) {
			const pointsDTO = new PointsDTO(points);
			pointsDTOList.push(pointsDTO);
		}
		this.pointsDTOList = pointsDTOList;
	}

	static toMultiPts(multiPtsDTO) {
		
		const pointsList = [];
		for(const pointsDTO of multiPtsDTO.pointsDTOList) {
			pointsList.push(PointsDTO.toPoints(pointsDTO));
		}
		return new MultiPts(pointsList, {color: multiPtsDTO.color, 
			visible: multiPtsDTO.visible, title: multiPtsDTO.title, 
			xAxis: multiPtsDTO.xAxis, yAxis: multiPtsDTO.yAxis});
	}
}
