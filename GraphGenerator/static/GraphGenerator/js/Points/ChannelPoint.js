import Point from "./Point";

export default class ChannelPoint extends Point{ 

	constructor(mass, energy, channel) {
		super(mass, energy);
		this._mass = super.x;
		this._energy = super.y;
		this._channel = channel;
	}

	get channelPoint() {
		return {channel: this._channel, mass: this._mass, energy: this._energy };
	}

	get mAndE() {
		return { mass: this.mass, energy: this.energy };
	}

	get channel() {
		return this._channel;
	}
	
	get mass() {
		return this._mass;
	}

	get energy() {
		return this._energy;
	}

	toString () {
		return `${this._mass}, ${this._energy}, ${this._channel}`;
	}
}
