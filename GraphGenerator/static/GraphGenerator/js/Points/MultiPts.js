import Points from "./Points";

export default class MultiPts extends Points{

	constructor (pointsList, {color = "Black", visible = false, title, xAxis, yAxis} = {}) {
		const pointList = [];
		const pointsSet = new Set();
		for (const points of pointsList) {
			pointList.push(...points);
			pointsSet.add(points);
		}
		super(pointList, {color: color, visible: visible, title: title, 
			xAxis: xAxis, yAxis: yAxis});
		this._pointsSet = pointsSet;
	}

	* points() {
		for(const points of this._pointsSet) {
			yield points;
		}
	}

	sub(func) {
		for(const points of this._pointsSet) {
			points.sub(func);
		}
		return super.sub(func);
	}

	drawData(ctx, position) {
		if(super.visible) {
			super.drawData(ctx, position);
		}
		else {
			for(const points of this._pointsSet) {
				points.drawData(ctx, position);
			}
		}
		this.drawSelections(ctx, position);
	}

	* titles() {
		for(const points of this._pointsSet) {
			yield points.title;
		}
	}
}
