export default class Point { 

	constructor(x, y) {
		this._x = x;
		this._y = y;
	}

	get point() {
		return {x: this._x, y: this._y};
	}

	get array() {
		return [this._x, this._y];
	}

	get x() {
		return this._x;
	}
	
	get y() {
		return this._y;
	}

	toString () {
		return `${this._x}, ${this._y}`; 
	}
}
