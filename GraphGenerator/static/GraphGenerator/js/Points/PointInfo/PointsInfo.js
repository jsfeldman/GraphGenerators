export default class PointsInfo {

	constructor ({color = "Black", visible = true, title = "", 
		xAxis = "", yAxis= ""} = {}) {
		this._color = color;
		this._visible = visible;
		this._title = title;
		this._xAxis = xAxis;
		this._yAxis = yAxis;
		this._selectionBoxes = new Set();
	}

	get color() {
		return this._color;
	}

	set color(color) {
		this._color = color;
	}

	get visible() {
		return this._visible;
	}

	set visible(visible) {
		this._visible = visible;
	}

	get title() {
		return this._title;
	}

	set title(title) {
		this._title = title;
	}

	get xAxis() {
		return this._xAxis;
	}

	set xAxis(xAxis) {
		this._xAxis = xAxis;
	}

	get yAxis() {
		return this._yAxis;
	}

	set yAxis(yAxis) {
		this._yAxis = yAxis;
	}

	get selectionBoxes() {
		return this._selectionBoxes;
	}

	static colors() {
		const COLORS = ["Aqua","PowderBlue", "Blue", "Fushsia", "Gray", "Green", 
			"Lime", "Maroon", "Navy", "Olive", "Purple", "Red", "Silver", "Teal", 
			"Yellow", "Lightsalmon"];

		return COLORS;
	}
}
