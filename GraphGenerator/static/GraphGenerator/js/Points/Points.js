import PointsInfo from "./PointInfo/PointsInfo";

export default class Points extends PointsInfo{

	constructor (points, {color, visible, title, xAxis, yAxis} = {}) {
		super({color: color, visible: visible, title: title, 
			xAxis: xAxis, yAxis: yAxis});

		this._lowestX = Number.POSITIVE_INFINITY;
		this._lowestY = Number.POSITIVE_INFINITY;
		this._highestX = Number.NEGATIVE_INFINITY;
		this._highestY = Number.NEGATIVE_INFINITY;

		this._pointList = [];
		for(const point of points) {
			this._pointList.push(point);
			this.setExtremes(point);
		}	
	}

	* [Symbol.iterator]() {
		for (const point of this._pointList) {
			yield point;
		}
	}

	* array() {
		for (const point of this._pointList) {
			yield point.array;
		}
	}

	drawData(ctx, position) {
		if(this.visible) {
			ctx.fillStyle = this.color;
			for(const point of this._pointList) {
				const [x, y] = position(point.x, point.y);
				ctx.beginPath();
				ctx.arc(x, y, 1, 0, 2 * Math.PI, true);
				ctx.fill();
				ctx.closePath();
			}
		}
	}

	drawSelections(ctx, position) {
		for(const [p0, p1] of this.selectionBoxes) {
			ctx.strokeStyle = "red";
			const [x0, y0] =  position(p0.x, p0.y);
			const [x1, y1] = position(p1.x, p1.y);
			ctx.strokeRect(x0, y0, x1 - x0, y1 - y0);
		}
	}

	setExtremes(point) {
		if(this._lowestX > point.x) {
			this._lowestX = point.x;
		}

		if(this._highestX < point.x) {
			this._highestX = point.x;
		}

		if(this._lowestY > point.y) {
			this._lowestY = point.y;
		}

		if(this._highestY < point.y) {
			this._highestY = point.y;
		}
	}

	sub(func) {
		const topic = `Points.${this.title}`;
		return PubSub.subscribe(topic, func);
	}

	registerInput(button, registerFunc) {
		registerFunc(button, this);
	}

	get pointsInfo() {
		return ({color: this.color, visible: this.visible, title: this.title, 
			xAxis: this.xAxis, yAxis: this.yAxis});
	}

	get lowestX() {
		return this._lowestX;
	}

	get highestX() {
		return this._highestX;
	}

	get lowestY() {
		return this._lowestY;
	}

	get highestY() {
		return this._highestY;
	}

	get length() {
		return this._pointList.length;
	}

	toString() {
		return this._pointList.map((point) => point.toString()).join("\n");
	}

}
