import Point from "./Point";
import Points from "./Points";

export default class PointsDTO {

	constructor(points) {

		this.lowestX = points.lowestX;
		this.lowestY = points.lowestY;
		this.highestX = points.highestX;
		this.highestY = points.highestY;
		this.color = points.color;
		this.visible = points.visible;
		this.title = points.title;
		this.xAxis = points.xAxis;
		this.yAxis = points.yAxis;
		const [...array] = points.array();
		this.points = array;
	}

	static toPoints(pointsDTO) {
		const pointList = [];
		for(const point of pointsDTO.points) {
			pointList.push(new Point(point[0], point[1]));
		}
		return new Points(pointList, {color: pointsDTO.color, 
			visible: pointsDTO.visible, title: pointsDTO.title, 
			xAxis: pointsDTO.xAxis, yAxis: pointsDTO.yAxis});
	}
}
