const PubSub = require("pubsub-js");

import GraphInfo from "./GraphInfo/GraphInfo";

export default class Graph extends GraphInfo{

	constructor(canvasId, {startX, startY, width, height, bgColor, graphColor,
		wdthBtwnT, hghtBtwnT, majorTW, majorTH, minorTW, minorTH, font, titleFS,
		axisFS} = {}
	) {
		super({startX: startX, startY: startY, width: width, height: height, 
			wdthBtwnT: wdthBtwnT, hghtBtwnT: hghtBtwnT, majorTW: majorTW, 
			majorTH: majorTH, minorTW: minorTW, minorTH: minorTH, font: font,
			titleFS: titleFS, axisFS: axisFS}
		);

		//Canvas
		this.plot = document.getElementById(canvasId);
		this.plotC = this.plot.getContext("2d");
		this.plot.width = this.width + (2 * 100);
		this.plot.height = this.height + (2 * 100);
		
		this.pointsToken = null;

		//Position Functions	
		const getAbsPos1D = 
			(start, loc, lowest, highest, scale) => (
				start + (((loc - lowest) / (highest - lowest)) * scale)
			);
		//Because canvas y starts at the top instead of bottom you need to flip
		//with the negative signs
		//Old code that explains what's going on a little better:
		//const y = ((this.startY + this.height) - 
		//	(getRelPos(point.y, this.lYP, this.hYP) * this.height)
		//);
		this.getAbsPos2D = (startX, lX, hX, width, startY, lY, hY, height) => 
			(x, y) => ([
				(getAbsPos1D(startX, x, lX, hX, width)),
				(-getAbsPos1D(-(startY + height), y, lY, hY, height))
				]
			);

		const getRelPos1D = (start, pixel, lowest, highest, scale) => (
			(((pixel - start) / scale) * (highest - lowest)) + lowest
		);
		this.getRelPos2D = (startX, lX, hX, width, startY, lY, hY, height) =>
			(x, y) => ([
				(getRelPos1D(startX, x, lX, hX, width)),
				(getRelPos1D(-(startY + height), -y, lY, hY, height))
				]
			);
	}

	drawGraph(color) {
		this.plotC.fillStyle = color; 
		this.plotC.strokeStyle = color; 

		this.plotC.beginPath();

		//Draw Frame
		this.plotC.lineWidth = 3;
		this.plotC.strokeRect(this.startX, this.startY, this.width, this.height);

		//Draw Ticks
		//Vertical Ticks
		for(let x = this.startX + this.wdthBtwnT, i = 1; x < this.startX + this.width; x += this.wdthBtwnT, i++) {
			if((i % 4 !== 0)) {
				this.plotC.moveTo(x, this.startY);
				this.plotC.lineTo(x, this.startY +	this.minorTH);
				this.plotC.moveTo(x, this.startY + this.height);
				this.plotC.lineTo(x, this.startY + this.height -	this.minorTH);
			} 
			else {
				this.plotC.lineWidth = this.majorTW;
				this.plotC.moveTo(x, this.startY);
				this.plotC.lineTo(x, this.startY + this.majorTH);
				this.plotC.moveTo(x, this.startY + this.height);
				this.plotC.lineTo(x, this.startY + this.height - this.majorTH);
				this.plotC.lineWidth = this.minorTW;
			}
		}

		//Horizontal Ticks
		for(let y = this.startY + this.height - this.hghtBtwnT, i = 1; y > this.startY; y -= this.hghtBtwnT, i++) {
			if((i % 4 !== 0)) {
				this.plotC.moveTo(this.startX, y);
				this.plotC.lineTo(this.startX + this.minorTH, y);
				this.plotC.moveTo(this.startX + this.width, y);
				this.plotC.lineTo(this.startX + this.width - this.minorTH, y);
			} 
			else {
				this.plotC.lineWidth = this.majorTW;
				this.plotC.moveTo(this.startX, y);
				this.plotC.lineTo(this.startX + this.majorTH, y);
				this.plotC.moveTo(this.startX + this.width, y);
				this.plotC.lineTo(this.startX + this.width - this.majorTH, y);
				this.plotC.lineWidth = this.minorTW;
			}
		}

		this.plotC.closePath();		
		this.plotC.stroke();
	}

	drawIndices(color) {
		this.plotC.fillStyle = color; 
		this.plotC.strokeStyle = color; 

		this.plotC.font = `${this.axisFS.toString()}px ${this.font}`;

		this.sizeX = (this.hX - this.lX) / (this.width / this.wdthBtwnT);
		this.sizeY = (this.hY - this.lY) / (this.height / this.hghtBtwnT);

		//Vertical Indices
		this.plotC.textBaseline = "hanging";
		this.plotC.textAlign = "center";
		for(let x = this.startX, indexNum = this.lX; x < this.startX + this.width; x += this.wdthBtwnT * 4, indexNum += this.sizeX * 4) {
			this.plotC.fillText(Math.round(indexNum), x, this.startY + this.height + this.majorTH);
		}

		//Horizontal Indices
		this.plotC.textBaseline = "middle";
		this.plotC.textAlign = "end";
		for(let y = this.startY + this.height, indexNum = this.lY; y > this.startY; y -= this.hghtBtwnT * 4, indexNum += this.sizeY * 4) {
			this.plotC.fillText((Math.round(indexNum * 10) / 10).toFixed(1), this.startX - this.majorTH, y);
		}
	}

	drawAxisNames(color) {
		this.plotC.strokeStyle = color; 
		this.plotC.font = `${this.axisFS.toString()}px ${this.font}`;

		//X Axis Name
		this.plotC.textBaseline = "Top";
		this.plotC.textAlign = "center";
		
		this.plotC.fillText(this.dataset.xAxis, this.startX + (this.width / 2), this.startY + this.height + this.axisFS + this.AxisNameDistFromIndices);

		//Y Axis Name
		this.plotC.textBaseline = "bottom";
		this.plotC.textAlign = "center";
		this.plotC.translate(this.startX - this.axisFS - this.AxisNameDistFromIndices, this.startY + (this.height / 2));	
		this.plotC.rotate(-(Math.PI / 2));
		this.plotC.fillText(this.dataset.yAxis, 0, 0);	

		this.plotC.setTransform(1, 0, 0, 1, 0, 0);
	}

	drawTitle(color) {
		this.plotC.fillStyle = color; 
		this.plotC.strokeStyle = color; 

		this.plotC.font = `${this.titleFS.toString()}px ${this.font}`;
		this.plotC.textBaseline = "bottom";
		this.plotC.textAlign = "center";
		this.plotC.fillText(this.dataset.title, this.startX + (this.width / 2), this.startY - this.majorTH);
	}

	drawData() {
		this.dataset.drawData(this.plotC, this.getAbsPosOfPoint);
	}

	drawBG(color) {
		this.plotC.fillStyle = color;
		this.plotC.fillRect(0, 0, this.plot.width, this.plot.height);
	}


	set dataset(data) {
		if(this.pointsToken === null) {
			this.pointsToken = data.sub((msg, data) => (this.makePlot()));
		}
		else {
			PubSub.unsubscribe("Points");
			this.pointsToken = data.sub((msg, data) => (this.makePlot()));
		}
		this._dataset = data;
		this.lX = data.lowestX; 
		this.hX = data.highestX; 
		this.lY = data.lowestY; 
		this.hY = data.highestY; 
		this.selectorBoxes = new Set();
		this.makePlot();
	}

	get dataset() {
		this.getAbsPosOfPoint = this.getAbsPos2D(this.startX, this.lX, this.hX, 
			this.width, this.startY, this.lY, this.hY, this.height
		);
		this.getRelPosOfPoint = this.getRelPos2D(this.startX, this.lX, this.hX,
			this.width, this.startY, this.lY, this.hY, this.height
		);
		return this._dataset;
	}

	registerInput(button, registerFunc) {
		registerFunc(button, this);
	}

	get whiteSpaceAmt() {

	}

	clearPlot() {
		this.plotC.clearRect(0, 0, this.plot.width, this.plot.height);
	}

	makePlot() {
		this.clearPlot();

		this.getAbsPosOfPoint = this.getAbsPos2D(this.startX, this.lX, this.hX, 
			this.width, this.startY, this.lY, this.hY, this.height
		);
		this.getRelPosOfPoint = this.getRelPos2D(this.startX, this.lX, this.hX,
			this.width, this.startY, this.lY, this.hY, this.height
		);

		this.drawBG(this.bgColor);
		this.drawGraph(this.graphColor);
		this.drawIndices(this.graphColor);
		this.drawTitle(this.graphColor);
		this.drawAxisNames(this.graphColor);
		this.drawData();
	}
}
