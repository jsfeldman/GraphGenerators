import PointsInfo from "./../../Points/PointInfo/PointsInfo.js";

export default class GraphInfo {

	constructor({startX = 100, startY = 100, width = 800, height = 400,
		bgColor = "White", graphColor = "Black", wdthBtwnT = 20, hghtBtwnT = 15, 
		majorTW = 5, majorTH = 5, minorTW = 3, minorTH = 3, font = "san-serif", 
		titleFS = 10, axisFS = 10}
	) {
		//Graph Information
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.height = height;
		this.bgColor = bgColor;
		this.graphColor = graphColor;
		this.wdthBtwnT = wdthBtwnT;
		this.hghtBtwnT = hghtBtwnT;
		this.majorTW = majorTW;
		this.majorTH = majorTH;
		this.minorTW = minorTW;
		this.minorTH = minorTH;

		//Text Style
		this.font = font;
		this.titleFS = titleFS;
		this.axisFS = axisFS;
		this.AxisNameDistFromIndices = 20;
		
		//Boxes
		this.selectorBoxes = new Set();
	}
	
	//Maybe if I'm not lazy I'll implement the rest one day
	/*get width() {
		return this._width;
	}

	set width(width) {
		this._width = width;
	}

	get height() {
		return this._height;
	}

	set height(height) {
		this._height = height;
	}

	get bgColor() {
		return this._bgColor;
	}

	set bgColor(bgColor) {
		this._bgColor = bgColor;
	}

	get graphColor() {
		return this._graphColor;
	}

	set graphColor(graphColor) {
		this._graphColor = graphColor;
	}*/
}
