//const PubSub = require("pubsub-js");

import DataFile from "./DataFile";
import RegisterGraphButtons from "./Inputs/Graph/RegisterGraphButtons";
import Graph from "./Graphs/Graph";

class Main {

	static init() {

		//Create GraphButton
		const graph = new Graph("MALDITOFScatterPlot"); 
		//Weird Bug where the subscription can't be created in the constructor
		PubSub.subscribe(`Graph.${graph.plot.id}`,
			(msg, data) => (graph.makePlot())
		);
		RegisterGraphButtons.register(graph);

		const dataElement = document.getElementById("data");
		dataElement.addEventListener("change", 
			(e) => DataFile.handleFiles(e, graph), false
		);

	}
}

window.onload = function() {
	Main.init();
};
