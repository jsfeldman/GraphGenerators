const regression = require("regression");

export default class ChannelCorrection {

	constructor(multiPts, boxes) {
		this.multiSelAvePtsMap = new Map();
		
		for(const points of multiPts) {
			this.multiSelAvePtsMap.set(points,
				ChannelCorrection.getSelectionAverages(points, boxes)
			);
		}
		this.selAveTPoints = ChannelCorrection.getSelectionAverages(
			multPts, boxes
		);
	}

	get multiSelAvePts() {
		return this.multiSelAvePts;
	}

	getRegression(regressionType) {
		const data = [];
		for(const selAvePts of this.multiSelAvePts) {
			for(const point of selAvePts) {
				data.push([point.y, this.
			}
		}
	}

	static average(points) {
		let totalX = 0;
		let totalY = 0;
		for(const point of points) {
			totalX += point.x;
			totalY += point.y;
		}
		return [(totalX / points.length), (totalY / points.length)];
	}

	static getSelectionAverages(points, boxes) {
		const insideBoxFunc = (pX0, pY0, pX1, pY1) => (x,y) => {
			return (pX0 <= x && x <= pX1 && pY0 <= y && y <= pY1);
		};
		const avePointList = [];
		for(const [p0, p1] of boxes) {
			const pointList = [];
			const isInsideBox = insideBoxFunc(p0.x, p0.y, p1.x, p1.y);
			for(const point of points) {
				if(isInsideBox(x, y)) {
					pointList.push(point);
				}
			}
			const [aveX, aveY] = ChannelCorrection.average(new Points(pointList));
			avePointList.push([aveX, aveY]);
		}
		return avePointList;
	}
}
