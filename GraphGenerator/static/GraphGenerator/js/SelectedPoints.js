const Regression = require("regression");

import Points from "./Points/Points";
import MultiPts from "./Points/MultiPts";

export default class SelectedPoints{

	constructor(multiPts, regressionType) {
		const boxes = multiPts.selectionBoxes;
		this.regressionType = regressionType;

		this.selectionMultiPts = new Set();
		for(const points of multiPts.points()) {
			this.selectionMultiPts.add(
				SelectedPoints.getSelections(points, boxes)
			);
		}
		this.totalSelections = SelectedPoints.getSelections(multiPts, boxes);
		
		this.multiSelectionAverages = new Map();
		for(const selectionPoints of this.selectionMultiPts) {
			const averageList = [];
			for(const selection of selectionPoints.points()) {
				if(selection.title !== "Not in box") {
					averageList.push(SelectedPoints.average(selection));
				}
			}
			this.multiSelectionAverages.set(selectionPoints.title, averageList);
		}
		this.totalSelectionAverages = [];
		for(const selection of this.totalSelections.points()) {
			this.totalSelectionAverages.push(SelectedPoints.average(selection));
		}	

		this.regressionDatum = new Map();
		for(const [title, selectionAverages] of this.multiSelectionAverages) {
			const data = [];
			for(let i = 0; i < selectionAverages.length; i++) {
				data.push([selectionAverages[i][1], 
					this.totalSelectionAverages[i][1]]
				);
			}
			this.regressionDatum.set(title, data);
		}
	}

	get getRegressions() {
		const regressions = new Map();
		for(const [title, regressionData] of this.regressionDatum) {
			let result;
			if(this.regressionType !== "polynomial") {
				result = new Regression(this.regressionType, regressionData);
			}
			else {
				result = new Regression(this.regressionType, regressionData, 
					regressionData.length
				);
			}
			regressions.set(title, result);
		}
		return regressions;
	}

	static average(points) {
		let totalX = 0;
		let totalY = 0;
		for(const point of points) {
			totalX += point.x;
			totalY += point.y;
		}
		return [(totalX / points.length), (totalY / points.length)];
	}

	static getSelections(points, selectorBoxes) {
		const NOT_IN_BOX = "Not in box";
		const insideBoxFunc = (pX0, pY0, pX1, pY1) => (x,y) => {
			return (pX0 <= x && x <= pX1 && pY1 <= y && y <= pY0);
		};
		const pointsMap = new Map();
		for(const box of selectorBoxes) {
			pointsMap.set(box, []);
		}
		pointsMap.set(NOT_IN_BOX, []);
		for(const point of points) {
			let inBox = false;
			for(const box of selectorBoxes) {
				const [p0, p1] = box;
				const isInsideBox = insideBoxFunc(p0.x, p0.y, p1.x, p1.y);
				if(isInsideBox(point.x, point.y)) {
					pointsMap.get(box).push(point);
					inBox = true;
				}
			}
			if(!inBox) {
				pointsMap.get(NOT_IN_BOX).push(point);
			}
		}
		const pointsList = [];
		const colors = Points.colors();
		for(const [box, pointList] of pointsMap) {
			const pointsInfo = points.pointsInfo;
			if(Array.isArray(box)) {
				const [p0, p1] = box;
				const title = `[[${p0.x}, ${p0.y}],[${p1.x},${p1.y}]]`;
				pointsInfo.title = title;
				pointsInfo.color = colors.pop();
			}
			else{
				pointsInfo.title = box;
				pointsInfo.color = "black";
			}
			pointsList.push(new Points(pointList, pointsInfo));
		}
		return new MultiPts(pointsList, points.pointsInfo);
	}
}
