import RegisterPointsButtons from "./../Inputs/Points/RegisterPointsButtons";
import SelectorInput from "./../Inputs/Points/SelectorInput";
import RegressionInput from "./../Inputs/Points/RegressionInput";

import ChannelPoint from "./../Points/ChannelPoint";
import Points from "./../Points/Points";
import MultiPts from "./../Points/MultiPts";

export default class MALDITOFFileLoader{

	static load (loadedFile, timeToMassFunc, graph) {
		loadedFile = loadedFile.trim();
		const splitFile = loadedFile.split("\n");
		const title = splitFile.shift().split("/").pop();
		const [xAxis, yAxis, channel] = splitFile.shift().split(";");

		const channelMap = new Map();

		for (const line of splitFile) {
			const [timeOfFlight, energy, channel] = line.split(";");
			const point = new ChannelPoint(
				timeToMassFunc(Number(timeOfFlight)), 
				Number(energy), 
				Number(channel)
			);
			let channelPoints;
			const channelTitle = "channel" + channel;
			if ((channelMap.has(channelTitle))) {
				channelPoints = channelMap.get(channelTitle);
				channelPoints.add(point);
			}
			else {
				channelPoints = new Set();
				channelPoints.add(point);
				channelMap.set(channelTitle, channelPoints);
			}
		}

		RegisterPointsButtons.setButtons();

		const pointsSet = new Set();
		const channelNames = new Set();
		for (const [channelName, channelPoints] of channelMap) {
			channelNames.add(channelName);
			const points = new Points([...channelPoints], {title: channelName, 
				xAxis: "m/z (kTh)", yAxis: "Relative Energy Deposited in Detector"}
			);
			RegisterPointsButtons.register(points);
			pointsSet.add(points);
		}

		const multiPts = new MultiPts([...pointsSet], {title: title, 
			xAxis: "m/z (kTh)", yAxis: "Relative Energy Deposited in Detector"}
		);

		//Will be fixed later
		multiPts.registerInput(graph, SelectorInput.registerFunc);

		const selectElement = document.getElementById("regression");
		multiPts.registerInput(selectElement, RegressionInput.registerFunc);

		return multiPts;
	}

}
