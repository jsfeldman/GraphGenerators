import MALDITOFFileLoader from "./FileLoaders/MALDITOFFileLoader"; 
import TOFFunctions from "./TOFFunctions";
import PointsDTO from "./Points/PointsDTO";
import MultiPtsDTO from "./Points/MultiPtsDTO";
import Graph from "./Graphs/Graph";
//import MyWorker from "./ClusterAlgorithms/HCluster.worker";

export default class DataFile {

	static handleFiles (evt, graph) {
		this.dataFiles = evt.target.files;
		const reader = new FileReader();
		reader.onload = function(event) {
			const multiPoints = MALDITOFFileLoader.load(event.target.result, 
				((time) => TOFFunctions.theoreticalEq(time, 1.566, 20000)), graph
			);
			graph.dataset = multiPoints;	
			/*const worker = new MyWorker();
			worker.onmessage = function (e) {
				const multiPts = MultiPtsDTO.toMultiPts(e.data);

				const canvas = document.createElement("canvas");
				const canvasId = multiPts.title + "plot";
				canvas.id = canvasId;
				canvas.width = "800";
				canvas.height = "400";
				document.body.append(canvas);

				const graph = new Graph(canvasId);
				graph.dataset = multiPts;
			};
			//Create SubGraphs (temporarily using this method)
/*			for(const points of multiPoints.points()) {
				if(points.length > 1000) {
					const pointsDTO = new PointsDTO(points);
					worker.postMessage(pointsDTO);
				}
			}*/
		};
		reader.onprogress = DataFile.updateProgress;
		for(const file of this.dataFiles) {
			reader.readAsText(file);
		}
	}
	
	static updateProgress (evt) {
		if (evt.lengthComputable) {
			let loaded = (evt.loaded / evt.total);
			//if (loaded < 1) {
				const progressBar = document.getElementById("csvProgressBar");
				console.log(Math.round(100 * loaded));
				progressBar.value = Math.round(100 * loaded);
			//}
		}
	}
}
