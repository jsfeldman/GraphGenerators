import GraphInput from "./GraphInput";

export default class GraphColorInput extends GraphInput {

	static registerFunc(button, graph){	
		button.addEventListener("change", () => {
			graph.graphColor = button.value;
			super.redrawGraph(graph.plot.id);
		}, false);
	}
}
