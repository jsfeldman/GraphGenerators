import GraphInput from "./GraphInput";

export default class BGColorInput extends GraphInput {

	static registerFunc(button, graph){	
		button.addEventListener("change", () => {
			graph.bgColor = button.value;
			super.redrawGraph(graph.plot.id);
		}, false);
	}
}
