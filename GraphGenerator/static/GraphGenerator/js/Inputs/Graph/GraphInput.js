import Input from "./../Input";

export default class GraphInput extends Input {

	//Graph inputs publish a message with "Graph-${canvas id}"
	static redrawGraph(canvasId) {
		super.redrawGraph(`Graph.${canvasId}`);
	}
}
