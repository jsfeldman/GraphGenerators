import GraphInput from "./GraphInput";

export default class WidthInput extends GraphInput {

	static registerFunc(button, graph){	
		button.addEventListener("change", () => {
			graph.width = parseInt(button.value);
			graph.plot.width = graph.width + (2 * 100);
			console.log(graph.plot.id);
			super.redrawGraph(graph.plot.id);
		}, false);
	}
}
