import RegisterButtons from "./../RegisterButtons";

import BGColorInput from "./BGColorInput";
import GraphColorInput from "./GraphColorInput";
import HeightInput from "./HeightInput";
import WidthInput from "./WidthInput";

export default class RegisterGraphButtons extends RegisterButtons {

	static register(graph) {
		const inputMap = new Map();
		inputMap.set("bgColor", BGColorInput);
		inputMap.set("graphColor", GraphColorInput);
		inputMap.set("height", HeightInput);
		inputMap.set("width", WidthInput);
		const divId = `${graph.plot.id}-GraphButtons`;
		const inputs = document.querySelectorAll(`div#${divId} > input`);
		super.register(graph, inputs, inputMap);
	}
}
