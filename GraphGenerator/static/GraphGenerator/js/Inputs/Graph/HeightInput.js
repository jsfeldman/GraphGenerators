import GraphInput from "./GraphInput";

export default class HeightInput extends GraphInput {

	static registerFunc(button, graph){	
		button.addEventListener("change", () => {
			graph.height = parseInt(button.value);
			graph.plot.height = graph.height + (2 * 100);
			super.redrawGraph(graph.plot.id);
		}, false);
	}
}
