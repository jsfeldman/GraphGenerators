export default class RegisterButtons {

	static register(object, inputs, inputMap) {
		for(const input of inputs) {
			for(const inputClass of input.classList) {
				if(inputMap.has(inputClass)) {
					const registerFunc = inputMap.get(inputClass).registerFunc;
					object.registerInput(input, registerFunc);
				}
			}
		}
	}
}
