import PointsInput from "./PointsInput";
import SelectedPoints from "./../../SelectedPoints";

import Point from "./../../Points/Point";
import Points from "./../../Points/Points";
import MultiPts from "./../../Points/MultiPts";
import Graph from "./../../Graphs/Graph";

export default class RegressionInput extends PointsInput {

	static registerFunc (selector, multiPts) {

		const linear = (gradient, yIntercept) => (x) => {
			return gradient * x + yIntercept;
		};

		const exponential = (a, b) => (x) => {
			return (a * Math.exp(b * x));
		};

		const logarithmic = (a, b) => (x) => {
			return (a + b * Math.log(x));
		};

		const power = (a, b) => (x) => {
			return (a * Math.pow(x, b));
		};

		const polynomial = (...as) => x => {
			return as.reduce((a, b, i) => (a + (b * Math.pow(x, i))), 0);
		};

		const regFormulas = new Map();
		regFormulas.set("linear", linear);
		regFormulas.set("exponential", exponential);
		regFormulas.set("logarithmic", logarithmic);
		regFormulas.set("power", power);
		regFormulas.set("polynomial", polynomial);

		//If it needs to be registered again then set selector value back to zero
		selector.value = "";
		const selectorFunc = ((evt) => {
			const parentNode = document.getElementById("channelCanvases");
			while(parentNode.firstChild) {
				parentNode.removeChild(parentNode.firstChild);
			}

			const regression = evt.target.value;
			const selectedPoints = new SelectedPoints(multiPts, regression);
			const regFormula = regFormulas.get(regression);

			const formulas = new Map();

			for(const [title, regression] of selectedPoints.getRegressions) {
				if(!isNaN(regression.equation[0])) {
					formulas.set(title, regFormula(...regression.equation));	
				}
			}

			const correctedPointsList = [];
			for(const points of multiPts.points()) {
				const formula = formulas.get(points.title);
				if(formula !== undefined) {
					const pointList = [];
					for(const point of points) {
						pointList.push(new Point(point.x, formula(point.y)));
					}
					const correctedPoints = new Points(pointList, points.pointsInfo);
					correctedPointsList.push(correctedPoints);
					const [...pointsCopyList] = points;
					const pointsCopy = new Points(pointsCopyList, {color: "lightgreen"});
					const multiPtsInfo = points.pointsInfo;
					multiPtsInfo.visible = false;
					const combinedPoints = new MultiPts([correctedPoints, pointsCopy], 
						multiPtsInfo
					);
					const c = document.createElement("canvas");
					parentNode.appendChild(c);
					c.id = `${points.title}-graph`;
					const graph = new Graph(c.id);
					graph.dataset = combinedPoints;
				}
			}
			const c = document.createElement("canvas");
			parentNode.insertBefore(c, parentNode.firstChild);
			c.id = `${multiPts.title}-graph`;
			const graph = new Graph(c.id);
			const correctedPoints = new MultiPts(correctedPointsList, 
				multiPts.pointsInfo
			);
			graph.dataset = correctedPoints;
		});

		selector.onchange = null;
		selector.onchange = selectorFunc;
	}
}
