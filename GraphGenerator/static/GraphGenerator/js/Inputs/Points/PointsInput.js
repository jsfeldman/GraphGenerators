import Input from "./../Input";

export default class PointsInput extends Input {

	//Point inputs publish a message with "Points-${title}"
	static redrawGraph(title) {
		super.redrawGraph(`Points.${title}`);
	}
}
