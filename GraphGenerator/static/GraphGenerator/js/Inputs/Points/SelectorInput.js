import PointInput from "./PointsInput";
import Point from "./../../Points/Point";

export default class SelectorInput extends PointInput {

	static registerFunc(graph, points){	
		let action = "";
		let rect = null;

		const diff = 5;

		const box = {
			x: 0,
			y: 0,
			x0: 0,
			y0: 0,
			x1: 0,
			x2: 0
		};

		const setBox = (x, y, x0, y0, x1, y1) => {
			box.x = x;
			box.y = y;
			box.x0 = x0;
			box.y0 = y0;
			box.x1 = x1;
			box.y1 = y1;
		};

		const element = graph.plot.parentElement;

		const mouseDownFunc = (evt) => {
			if(evt.button === 0) {
				const nearPoint = (x, y, pX, pY) => {
					return ((pX - diff) <= x) && (x <= (pX + diff)) && 
						((pY - diff) <= y) && (y <= (pY + diff));
				};
				const insideBox = (x, y, pX0, pY0, pX1, pY1) => {
					return ((pX0 + diff) <= x) && (x <= (pX1 - diff)) && 
						((pY0 + diff) <= y) && (y <= (pY1 - diff));
				};

				const x = evt.pageX - evt.currentTarget.offsetLeft;
				const y = evt.pageY - evt.currentTarget.offsetTop;

				rect = document.createElement("span");
				rect.className = "rect";
				//Breaks needed so multiple boxes aren't selected and one disappears
				for(const box of points.selectionBoxes) {
					const [pX0, pY0] = graph.getAbsPosOfPoint(box[0].x, box[0].y);
					const [pX1, pY1] = graph.getAbsPosOfPoint(box[1].x, box[1].y);
					if(nearPoint(x, y, pX0, pY0)) {
						points.selectionBoxes.delete(box);
						graph.plot.style.cursor = "nwse-resize";
						action = "resizeTL";
						setBox(x, y, pX0, pY0, pX1, pY1);
						break;
					}
					else if(nearPoint(x, y, pX1, pY1)) {
						points.selectionBoxes.delete(box);
						graph.plot.style.cursor = "nwse-resize";
						action = "resizeBR";
						setBox(x, y, pX0, pY0, pX1, pY1);
						break;
					}
					else if(insideBox(x, y, pX0, pY0, pX1, pY1)) {
						points.selectionBoxes.delete(box);
						if(evt.ctrlKey) {
							action = "delete";
							rect = null;
						}
						else {
							graph.plot.style.cursor = "move";
							action = "move";
							setBox(x, y, pX0, pY0, pX1, pY1);
						}
						break;
					}
				}
				if(!(action === "resizeTL" || action === "resizeBR" || 
					action === "move" || action === "delete")
				) {
					graph.plot.style.cursor = "crosshair";
					action = "create";
					setBox(x, y, 0, 0, 0, 0);
				}

				super.redrawGraph(points.title);
			}
		};

		const mouseMoveFunc = (evt) => {
			if(rect !== null) {
				graph.plot.parentElement.appendChild(rect);
				const x = evt.pageX - evt.currentTarget.offsetLeft;
				const y = evt.pageY - evt.currentTarget.offsetTop;
				if(action === "resizeTL") {
					rect.style.left = `${Math.min(x, box.x1)}px`; 
					rect.style.top = `${Math.min(y, box.y1)}px`;
					rect.style.width = `${Math.abs(x - box.x1)}px`;
					rect.style.height = `${Math.abs(y - box.y1)}px`;
				}
				else if(action === "resizeBR") {
					rect.style.left = `${Math.min(box.x0, x)}px`;
					rect.style.top = `${Math.min(box.y0, y)}px`;
					rect.style.width = `${Math.abs(box.x0 - x)}px`;
					rect.style.height = `${Math.abs(box.y0 - y)}px`;
				}
				else if(action === "move") {
					rect.style.left = `${(box.x0 + (x - box.x))}px`;
					rect.style.top = `${(box.y0 + (y - box.y))}px`;
					rect.style.width = `${(box.x1 - box.x0)}px`;
					rect.style.height = `${(box.y1 - box.y0)}px`;
				}
				else if(action === "create") {
					rect.style.left = `${Math.min(box.x, x)}px`;
					rect.style.top = `${Math.min(box.y, y)}px`;
					rect.style.width = `${Math.abs(box.x - x)}px`;
					rect.style.height = `${Math.abs(box.y - y)}px`;	
				}
			}
		};

		const mouseUpFunc = (evt) => {
			if(rect !== null && element.contains(rect)) {
				const [x0, y0] = [parseInt(rect.style.left), 
					parseInt(rect.style.top)
				];
				const [pX0, pY0] = new graph.getRelPosOfPoint(x0, y0);

				const [x1, y1] = [x0 + parseInt(rect.style.width),
					y0 + parseInt(rect.style.height)
				];
				const [pX1, pY1] = new graph.getRelPosOfPoint(x1, y1);

				points.selectionBoxes.add([new Point(pX0, pY0), new Point(pX1, pY1)]);
				element.removeChild(rect);

				rect = null;
				super.redrawGraph(points.title);
			}
			graph.plot.style.cursor = "default";
			action = "";
		};

		element.onmousedown = null;
		element.onmousemove = null;
		element.onmouseup = null;

		element.onmousedown = mouseDownFunc;
		element.onmousemove = mouseMoveFunc;
		element.onmouseup = mouseUpFunc;
	}
}
