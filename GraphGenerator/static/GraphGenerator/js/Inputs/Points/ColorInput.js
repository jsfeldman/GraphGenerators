import PointsInput from "./PointsInput";

export default class ColorInput extends PointsInput {



	//Done this way for now so they can be unregistered
	//Should have it so points keeps track of the functions it has registed
	//And can unregister them when data is being switched 
	//but it's not implemented yet
	static registerFunc(button, points) {	
		const colorFunc = (() => {
			points.color = button.value;
			super.redrawGraph(button.id);
		});

		const visibleFunc = ((evt) => {
			const visibleClass = "channelVisible";
			if(evt.which == 3) {
				button.classList.toggle(visibleClass);
				points.visible = !points.visible;	
				super.redrawGraph(button.id);
			}
		});

		button.onchange = null;
		button.onmousedown = null;

		button.onchange = colorFunc;
		button.onmousedown = visibleFunc;
	}
}
