import RegisterButtons from "./../RegisterButtons";

import ColorInput from "./ColorInput";

export default class RegisterPointsButtons extends RegisterButtons {

	static register(points) {
		const inputMap = new Map();
		inputMap.set("color", ColorInput);
		const input = document.getElementById(points.title);
		if(input !== null) {
			if(points.visible) {
				input.classList.add("channelVisible");
			}
			input.disabled = false;
			super.register(points, [input], inputMap);
		}
	}

	//Get the button's prepped based on points
	static setButtons () {

		//Because javascript doesn't have attributes to the class
		const CHANNEL_LIST = [
			"channel0",
			"channel1",
			"channel2",
			"channel3",
			"channel4",
			"channel5",
			"channel6",
			"channel7",
			"channel8",
			"channel9",
			"channel10",
			"channel11",
			"channel12",
			"channel13",
			"channel14",
			"channel15"
		];

		for (const channel of CHANNEL_LIST) {
			const el = document.getElementById(channel);	
			el.classList.remove("channelVisible");
			el.disabled = true;
			el.value = "#000000";
		}
	}
}
