export default class Input {
	static redrawGraph(msg) {
		const REDRAW_GRAPH = "Redraw Graph";
		PubSub.publish(msg, REDRAW_GRAPH);
	}
}
