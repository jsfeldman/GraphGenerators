var path = require('path');
//For if I ever decide to implement the plugin
//but I believe it requires not using the django paradigm of adding scripts
//var HtmlWebpackPlugin= require('html-webpack-plugin');

/*var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
	template: "./GraphGenerator/templates/GraphGenerator/index.html",
	filename: 'index.html'
});*/

module.exports = {
	entry: [
		"babel-polyfill",
		"./GraphGenerator/static/GraphGenerator/js/Main.js"
	],
	output : {
		filename: "./GraphGenerator/static/GraphGenerator/js/bundle.js",
	},
	devtool: 'source-map',
	module: {
		preLoaders: [
			{
				test: /\.js/,
				exclude:  /node_modules/,
				loader: 'jshint-loader'
			}
		],
		loaders: [
			{
  			test: [/\.js$/],
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'stage-0']
				}
			},
			{
				test: [/\.worker.js$/],
				loader: 'worker-loader?inline!babel?presets=es2015',
			}
		]
	},
	worker: {
		output: {
			filename: "./GraphGenerator/static/GraphGenerator/js/hash.worker.js",
			chunkFilename: "./GraphGenerator/static/GraphGenerator/js/[id].hash.worker.js"
		}
	},
	resolve: {
		extensions: ['','.js', '.es6', '.worker.js']
	} //,
	//plugins: [HTMLWebpackPluginConfig]
};

